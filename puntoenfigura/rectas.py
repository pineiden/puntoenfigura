import math
import random 

def pertenencia_intervalo(valor, intervalo):
	return valor >= intervalo.get('minimo') and valor <= intervalo.get('maximo')

def parametros_recta(punto_1, punto_2):
	# pendiente
	m = 0
	c = 0
	resultado = None
	if not (punto_1[0] >= punto_2[0] and punto_1[0] <= punto_2[0]):
		m = (punto_2[1]-punto_1[1]) / (punto_2[0]-punto_1[0])
		c = punto_1[1]-m*punto_1[0]
		resultado = (m,c)
	else:
		resultado = (0, punto_1[0])
	return resultado	

def rectas_cuadrilatero(cuadrilatero):
	# toma un cuadrilatero y calcula las rectas de cada par de puntos
	L1 = parametros_recta(*cuadrilatero[0])
	L2 = parametros_recta(*cuadrilatero[1])
	return (L1,L2)
	
def chequeo_pertenencia(punto, rectas):
	y = punto[1]
	valor_y = lambda punto, recta: recta[0]*punto[0]+recta[1]
	y_l1 = valor_y(punto, rectas[0])
	y_l2 = valor_y(punto, rectas[1])	
	chequeo_l = (y>=y_l1 and y<=y_l2) or (y>=y_l2 and y<=y_l1)	
	return chequeo_l


