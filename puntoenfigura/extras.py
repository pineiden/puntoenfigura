import math
import random 

def float_range(inicio, final, step):
	menor = inicio
	mayor = final
	if inicio > final:
		menor = final 
		mayor = inicio
	r = menor
	while r < mayor:
		yield r
		r += step

def random_point(boundaries, step = 0.1):
	r0 = random.choice(list(
		float_range(boundaries[0][0], boundaries[1][0], step)))
	r1 = random.choice(list(
		float_range(boundaries[0][1], boundaries[1][1], step)))
	return (r0, r1)


