import math
import random 

def pares_contiguos(valor, valor_fig):
	conjunto_index = []
	for i in range(len(valor_fig)-1):
		if (valor>= valor_fig[i] and valor <= valor_fig[i+1]) or  (valor >= valor_fig[i+1] and valor <= valor_fig[i]) :
			conjunto_index.append((i, i+1))
	if (valor>= valor_fig[-1] and valor <= valor_fig[1]) or  (valor >= valor_fig[1] and valor <= valor_fig[-1]):
		conjunto_index.append((len(valor_fig), 1))
	return conjunto_index

def delta_puntos(referencia, puntos):
	delta = lambda punto_0, punto: abs(punto[1]-punto_0[1])
	d1 = delta(referencia[0], puntos[0]) 
	d2 = delta(referencia[0], puntos[1]) 
	d3 = delta(referencia[1], puntos[0]) 
	d4 = delta(referencia[1], puntos[1]) 
	m1 = min([d1,d2])
	m2 = min([d3,d4])
	min_tot = min([m1,m2])
	return min_tot	
	
def cuadrilateros(pares_contiguos):
	# definimos conjunto set 
	# que permitirá guardar aquellos pares de puntos
	# que ya pertenecen a un cuadrilatero
	# definido por un conjunto de 2 pares contiguos
	reserva = set()
	# definimos lista 
	# que contienen los 2 pares contiguos
	# con determinan un cuadrilatero
	# que es contenido en el área de la figura
	cuadrilateros = []
	# leemos la lista 
	for par_contiguo in pares_contiguos:
		referencia = par_contiguo
		if not referencia in reserva:		
			deltas = []
			for par in pares_contiguos:
				# por cada par en pares contiguis
				# que el par no sea el mismo de referencia
				# y no esté en reserva
				
				# que si delta = 0
				# revisar caso!
				# es decir dos rectas se unen en un punto
				if not par == referencia and not par in reserva:
					delta = delta_puntos(referencia, par)
					deltas.append((par, delta))
			# determinamos el par continuo que define una recta mas
			# cercana al par de referencia
			if deltas:
				par_cuad = min(deltas, key=lambda x: x[1])
				# añadimos los pares a reserva
				reserva.add(referencia)
				reserva.add(par_cuad)
				# añadimos los pares a la lista 
				# de cuadrilaterios ya definidos
				cuadrilateros.append((referencia, par_cuad[0]))			
	return cuadrilateros
	
