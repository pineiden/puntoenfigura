import math
import random 
from extras import float_range, random_point


def crear_plano(max_value):
	rp = lambda: random.choice(range(-max_value, max_value))
	a = rp()
	b = rp()
	boundaries = [(0,0),(a,b)]	
	return a,b, boundaries
	
	
def crear_circulo(c0, c1, a, b):
	# calculamos la distancia minima desde el centro al borde
	xd1 = min([abs(c0-a), abs(c0)])
	yd1 = min([abs(c1-b), abs(c0)])
	# calculamos el máximo radio posible que permita
	# al círculo inscribirse en la zona
	max_radio = min(xd1, yd1)
	# obtener los puntos del círculo
	# rho = radio
	# (x, y) = (rho*cos(theta), rho*sin(theta)]
	# con theta = [0, 2*pi)
	# cada .1 radian
	x = lambda rho, theta: rho*math.cos(theta)
	y = lambda rho, theta: rho*math.sin(theta)
	rho = random.uniform(0,max_radio)
	puntos = [(x(rho, theta), y(rho, theta)) for theta in float_range(0, 2*math.pi, .1)]
	return puntos, rho

