import math
import random 
from creafigura import crear_plano, crear_circulo
from extras import float_range, random_point
from poligonales import (pares_contiguos, 
						delta_puntos,
						cuadrilateros)
from rectas import (pertenencia_intervalo, parametros_recta,
					rectas_cuadrilatero, chequeo_pertenencia)

max_value = 1000

a, b, boundaries = crear_plano(max_value)
# Construcción de un círculo inscrito en la zona
# obtener puntos aleatorios
step = 0.01
c0, c1 = random_point(boundaries, step)
puntos, radio =  crear_circulo(c0, c1, a, b)
# Ahora, buscamos un punto al azar.
x_rand, y_rand  =  random_point(boundaries)
punto_rand = (x_rand, y_rand)
print("Punto %s %s " %(x_rand, y_rand))
# La pregunta, el punto pertenece al área cuadrada
# que define el circulo, es decir se reduce el área de búsqueda
# según eje x, veamos que pertenezca a algún
# valor entre el min_x y max_x de la figura
# separamos la lista de valores en eje x y del eje y
# usamos la funcion zip que permite separar los
# elementos de la tupla en listas
x_fig, y_fig = zip(*puntos)
# obtenemos los valores que determinan
# el recuadro de zona en que la figura está inscrita

intervalo_x = dict(minimo=min(x_fig), maximo=max(x_fig))
intervalo_y = dict(minimo=min(y_fig), maximo=max(y_fig))

axis_x_boolean = pertenencia_intervalo(x_rand, intervalo_x)
axis_y_boolean = pertenencia_intervalo(y_rand, intervalo_y)

pertenencia = lambda eje_x, eje_y: eje_x and eje_y

# esto verifica que el punto pertenezca al área
# cuadriculada que define la figura

pertenencia_zona = pertenencia(axis_x_boolean, axis_y_boolean)

print("La figura se define por un círculo de radio %f y centro (%f, %f)" % (radio, c0, c1))

if pertenencia_zona:
	# ahora verificamos con precisión la pertenencia
	# a la figura en particular	
	# acotamos la zona, encontramos los cuadrilateros
	# extraemos la lista de indices en que
	# hay contiguidad al punto buscado
	pares_index = pares_contiguos(x_rand, x_fig)
	# se extrae la lista de puntos
	# en base a la lista de indices
	pares = [ (puntos[p1], puntos[p2]) for p1, p2 in pares_index ]
	# se definen los cuadrilateros que definen los pares 
	# contiguos
	lista_cuadrilateros = cuadrilateros(pares)
	lista_chequeo = []
	for cuadrilatero in lista_cuadrilateros:
		# se extraen las rectasque definen los cuadrilateros
		rectas = rectas_cuadrilatero(cuadrilatero)
		# se verifica pertenencia del punto a las rectas
		# segun eje y
		pertenencia = chequeo_pertenencia((x_rand, y_rand),rectas)
		lista_chequeo.append(pertenencia)
	# se hace un and entre cada elemento, resultado booleano
	resultado = any(lista_chequeo)
	if resultado:
		print("El punto (%f, %f) pertenece a la figura" %(x_rand, y_rand))
	
	else:
		print("El punto (%f, %f) no pertenece a la figura, pero es cercano" %(x_rand, y_rand))
	
else:
	print("El punto (%f, %f) no pertenece a la zona que enmarca la figura figura" %(x_rand, y_rand))
	
	
	
	

